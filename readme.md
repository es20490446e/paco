```
USAGE
       paco [operation] [targets]

OPERATIONS
       - "-add" or "-remove" for targets.

       - "-config" for configuring paco.

       - "-test" for running unit tests, using the remote recipe "0-test" (see
       "man testsh").

       - If empty, "-add" is assumed.

TARGETS
       -  If  empty, the packages or their PKGBUILD are seached in the current
       dir.

       - If set, the specified PKGBUILDs are searched in  the  remote  reposi‐
       tory.

       - If set as "all", all available PKGBUILDs in the remote repository are
       assumed.

EXIT STATUS
       2: If some targets failed
